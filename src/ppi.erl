-module(ppi).

%% API exports
-export([main/1, process_rec/3, send_py_files/6]).

%%====================================================================
%% API functions
%%====================================================================

%% escript Entry point
main(Args) ->
    OptSpecList = option_spec_list(),
    io:format("~p~n", [init:get_arguments()]),
    case getopt:parse(OptSpecList, Args) of
        {ok, {Options, _NonOptArgs}} ->
            Reqs = find_recs(Options),
            case file:open(Reqs, [read]) of
                {ok, Fptr} ->
                    ok = application:start(inets),
                    ok = ssl:start(),
                    Profile = ppi,
                    {ok, _Pid} = inets:start(httpc, [{profile, Profile}]),
                    ok = httpc:set_options([{ipfamily, inet}]),
                    spawn(ppi_csv, parse_rec, [self(), {[], [], {more, Fptr}}]),
                    case python_site() of
                        {ok, Site} ->
                            start_workers(0, no, no, Site),
                            erlang:halt(0);
                        {error, Error} ->
                            io:fwrite(standard_io, "Cannot find Python ~w~n", [Error]),
                            %% enoent
                            erlang:halt(34)
                    end;
                {error, Error} ->
                    io:fwrite(standard_io, "Cannot open ~s: ~w~n", [Reqs, Error]),
                    erlang:halt(1)
            end;
        {error, {Reason, Data}} ->
            io:fwrite(standard_io, "Error: ~s ~p~n~n", [Reason, Data]),
            getopt:usage(OptSpecList, "ex1"),
            erlang:halt(2)
    end.

%%====================================================================
%% Internal functions
%%====================================================================

option_spec_list() ->
    [
     {help, $?, "help", undefined, "Show the program options"},
     {reqs, $r, "reqs", {string, "reqs.csv"},
      "File with requierements.
The meaning fo columns: URL, SHA256, Keep Meta, Keep Scripts, Keep Sources"}
     ].

find_recs([{reqs, Reqs} | _]) -> Reqs;
find_recs([_ | More]) -> find_recs(More).

%% These files aren't usually necessry after install / during normal
%% operation, but some packages put them in. Speccifically, dist-info
%% is necessary for installers like `pip' to figure out the
%% dependencies, but we don't care about that.
%% We will also by default exclude pre-compiled bytecompiled files
%% and tests.
%%
%% Also, some wheels include directories, but we'll create them
%% anyways.
file_matchers(meta) ->
    lists:flatten([
                   "(^[^/]+\\.dist-info/)|",
                   "(.+\\.(p(y[ix]|xd)|typed)$)|",
                   "(.*__pycache__.*)|",
                   "(.*/tests/.*)|"
                   "(.*/$)"]).

%% TODO(crabbone): figure out where scripts are stored when they are
%% stored separately

%% file_matchers(script) -> "[^/]+\\.dist-info/";


python_site() ->
    Python = os:find_executable("python"),
    Opts = [
            {args, ["-c", "import sysconfig;print(sysconfig.get_path('platlib'))"]},
            exit_status,
            stderr_to_stdout
           ],
    Port = open_port({spawn_executable, Python}, Opts),
    get_data(Port, [], false, false).

get_data(P, D, Exited, Read) ->
    receive
        {P, {data, D1}} ->
            get_data(P, [D | D1], Exited, Read);
        {P, eof} ->
            case Exited of
                false ->
                    port_close(P),
                    get_data(P, D, false, true);
                _ -> {Exited, string:trim(lists:flatten(D), both)}
            end;
        {P, {exit_status, N}} ->
            Message = case N of 0 -> ok; _ -> error end,
            self() ! {P, eof},
            get_data(P, D, Message, Read)
    end.

should_use([], _) -> true;
should_use([{K, P} | Ks], File) ->
    case K of
        true ->
            case re:run(File, P) of
                {match, _} -> false;
                _ -> should_use(Ks, File)
            end;
        _ -> should_use(Ks, File)
    end.

send_py_file(Port, _, _, []) ->
    Port ! {self(), {command, "\n"}},
    ok;
send_py_file(Port, SrcDir, DestDir, [F | Files]) ->
    Port ! {self(), {command, lists:flatten([SrcDir, ":", DestDir, ":", F, "\n"])}},
    send_py_file(Port, SrcDir, DestDir, Files).

send_py_files(Python, Opts, SrcDir, DestDir, Fs, Pid) ->
    Port = open_port({spawn_executable, Python}, Opts),
    send_py_file(Port, SrcDir, DestDir, Fs),
    Received = get_data(Port, [], false, false),
    Pid ! Received.

pycompile_many(SrcDir, DestDir, Files) ->
    Python = os:find_executable("python"),
    Cmd = "import sys
from os.path import join, splitext
from py_compile import compile
for line in sys.stdin:
    if not line.strip():
        break
    src, dst, f = line.strip().split(':')
    d = join(dst, splitext(f)[0] + '.pyc')
    compile(join(src, f), cfile=d)
    print(d)
",
    Opts = [{args, ["-c", Cmd]}, exit_status, stderr_to_stdout],
    spawn(ppi, send_py_files, [Python, Opts, SrcDir, DestDir, Files, self()]),
    receive
        ok -> ok;
        Err -> Err
    end.

ensure_write_file(Tdir, File, Bin) ->
    Path = lists:flatten([Tdir, "/" | File]),
    case filelib:ensure_dir(Path) of
        ok ->
            case file:write_file(Path, Bin()) of
                ok -> ok;
                {error, Err} -> {err, {write_file, Err, Path}}
            end;
        {error, Err} -> {err, {ensure_dir, Err, Path}}
    end.

unzip_wheel_handler(Matchers, Tdir, Site, Src) ->
    fun(File, _Info, Bin, Acc) ->
            {{py, PyAcc}, {data, DataAcc}, {err, ErrAcc}} = Acc,
            case should_use(Matchers, File) of
                true ->
                    IsPySrc = re:run(File, ".+\\.py$"),
                    case {IsPySrc, Src} of
                        {{match, _}, true} ->
                            case ensure_write_file(Tdir, File, Bin) of
                                {err, Error} ->
                                    {{py, PyAcc}, {data, DataAcc}, {err, [Error | ErrAcc]}};
                                ok -> {{py, [File | PyAcc]}, {data, DataAcc}, {err, ErrAcc}}
                            end;
                        _ -> 
                            case ensure_write_file(Site, File, Bin) of
                                {err, Error} ->
                                    {{py, PyAcc}, {data, DataAcc}, {err, [Error | ErrAcc]}};
                                ok -> {{py, PyAcc}, {data, [File | DataAcc]}, {err, ErrAcc}}
                            end
                    end;
                _ -> Acc
            end
    end.

unzip_wheel(Payload, Req, Site) ->
    {Url, _, Meta, Script, Src} = Req,
    Matchers = [{Meta, file_matchers(meta)}],
    %% We don't care about non-Linux users
    Tdir = string:trim(os:cmd("mktemp -d"), both),
    Handler = unzip_wheel_handler(Matchers, Tdir, Site, Src),
    Acc = {{py, []}, {data, []}, {err, []}},
    case zip:foldl(Handler, Acc, {Url, Payload}) of
        {ok, Processed} ->
            {{py, PyP}, _, {err, ErrorP}} = Processed,
            case ErrorP of
                [] -> pycompile_many(Tdir, Site, PyP);
                _ -> {err, {unzip_wheel, ErrorP}}
            end;
        {error, Error} -> {err, {unzip_wheel, Error}}
    end.

process_rec(Pid, Req, Site) ->
    HttpOpts = [{connect_timeout, 1000}, {ssl, [{verify, verify_none}]}],

    %% zip doesn't work with partial data anyways, so we might as well
    %% read everything in
    Opts = [{body_format, binary}],
    {Url, Cs, _, _, _} = Req,
    case httpc:request(get, {Url, []}, HttpOpts, Opts) of
        {ok, {{_Proto, Status, Message}, _Headers, Payload}} ->
            case lists:member(Status, [200, 206]) of
                true ->
                    ShaInt = binary:decode_unsigned(crypto:hash(sha256, Payload)),
                    ComputedSha256 = io_lib:format("~64.16.0b", [ShaInt]),
                    case ComputedSha256 of
                        Cs ->
                            case unzip_wheel(Payload, Req, Site) of
                                %% TODO(crabbone): Printing PyCompOut might be
                                %% an interesting diagnostic message.
                                {ok, _PyCompOut} -> Pid ! {done, Url};
                                Error -> Pid ! {err, {missing_req, Url}, Error}
                            end;
                        _ -> Pid ! {err, {missing_req, Url}, checksum}
                    end;
                _ -> Pid ! {err, {missing_req, Url}, {Status, Message}}
              end;
        {error, Error} -> Pid ! {err, {missing_req, Url}, Error}
    end.

start_workers(N, Started, Finished, Site) ->
    io:fwrite("started_workers ~.b, ~w, ~w~n", [N, Started, Finished]),
    case {N, Started, Finished} of
        {0, yes, yes} -> ok;
        _ ->
            receive
                eof ->
                    io:fwrite("Finished reading reqs~n", []),
                    start_workers(N, Started, yes, Site);
                {req, Req} ->
                    io:fwrite("Accepted req: ~p~n", [Req]),
                    spawn(ppi, process_rec, [self(), Req, Site]),
                    start_workers(N + 1, yes, Finished, Site);
                {done, Req} ->
                    io:fwrite("Processed req: ~s~n", [Req]),
                    start_workers(N - 1, Started, Finished, Site);
                {err, Err, Reason} ->
                    io:fwrite(standard_io, "~w because: ~w~n", [Err, Reason]),
                    erlang:halt(4)
            end
    end.
