-module(ppi_csv).

-export([parse_line/3, parse_rec/2]).


lazy_read({_, [], {more, IoDevice}}) ->
    case file:read(IoDevice, 512) of
        {ok, Buf} ->
            [B | Bs] = Buf,
            {B, Bs, {more, IoDevice}};
        eof -> eof
    end;
lazy_read({_, [Y | Ys], Zs}) -> {Y, Ys, Zs};
lazy_read({_, eof, eof}) -> eof.

rope_prepend(eof, C) -> {C, eof, eof};
rope_prepend({X, Y, Z}, C) -> {C, [X | Y], Z}.

rope_next(IO) ->
    case IO of
        eof -> eof;
        {[], Y, Z} ->
            Next = lazy_read({[], Y, Z}),
            rope_next(Next);
        {X, Y, Z} -> {X, lazy_read({X, Y, Z})}
    end.

read_quot(Rope, nil, SoFar) ->
    case rope_next(Rope) of
        {$", Rest} -> {SoFar, Rest};
        {$\\, Rest} -> read_quot(Rest, esc, [SoFar, $\\]);
        {R, Rest} -> read_quot(Rest, nil, [SoFar, R]);
        eof -> eof
    end;
read_quot(Rope, esc, SoFar) ->
    case rope_next(Rope) of
        {$", Rest} -> read_quot(Rest, nil, [SoFar, $"]);
        {$\\, Rest} -> read_quot(Rest, nil, [SoFar, $\\]);
        {R, Rest} -> read_quot(Rest, nil, [SoFar, R]);
        eof -> eof
    end.

read_apos(Rope, nil, SoFar) ->
    case rope_next(Rope) of
        {$', Rest} -> {SoFar, Rest};
        {$\\, Rest} -> read_apos(Rest, esc, [SoFar, $\\]);
        {R, Rest} -> read_apos(Rest, nil, [SoFar, R]);
        eof -> eof
    end;
read_apos(Rope, esc, SoFar) ->
    case rope_next(Rope) of
        {$', Rest} -> read_apos(Rest, nil, [SoFar, $']);
        {$\\, Rest} -> read_apos(Rest, nil, [SoFar, $\\]);
        {R, Rest} -> read_apos(Rest, nil, [SoFar, R]);
        eof -> eof
    end.

read_to_col(Rope) ->
    case rope_next(Rope) of
        {$\n, Rest} -> {endl, Rest};
        {$\r, Rest} -> {endl, Rest};
        {$,, Rest} -> {comma, Rest};
        {R, Rest} -> {err, {read_to_col, rope_prepend(Rest, R)}};
        eof -> eof
    end.

read_to_space(Rope, nil, SoFar) ->
    case rope_next(Rope) of
        {$\s, Rest} -> {lists:reverse(SoFar), Rest};
        {$\t, Rest} -> {lists:reverse(SoFar), Rest};
        {$\n, Rest} -> {lists:reverse(SoFar), rope_prepend(Rest, $\n)};
        {$\r, Rest} -> {lists:reverse(SoFar), rope_prepend(Rest, $\r)};
        {$,, Rest} -> {lists:reverse(SoFar), rope_prepend(Rest, $,)};
        {$\\, Rest} -> read_to_space(Rest, esc, [$\\ | SoFar]);
        {R, Rest} -> read_to_space(Rest, nil, [R | SoFar]);
        eof -> eof
    end;
read_to_space(Rope, esc, SoFar) ->
    case rope_next(Rope) of
        {R, Rest} -> read_to_space(Rest, nil, [R | SoFar]);
        eof -> eof
    end.

read_string(Rope) ->
    case rope_next(Rope) of
        {$,, R} -> {[], rope_prepend(R, $,)};
        {$\n, R} -> {[], rope_prepend(R, $\n)};
        {$\r, R} -> {[], rope_prepend(R, $\r)};
        {$", R} -> read_quot(R, nil, []);
        {$', R} -> read_apos(R, nil, []);
        {X, R} ->
            case read_to_space(R, nil, []) of
                {Y, Z} -> {[X | Y], Z};
                eof -> eof
            end;
        eof -> eof
    end.

read_white(Rope) ->
    case rope_next(Rope) of
        {$\s, R} -> read_white(R);
        {$\t, R} -> read_white(R);
        {S, R} -> rope_prepend(R, S);
        eof -> eof
    end.

parse_line(IO, Result, State) ->
    %% '   '    '"x"'    '   '    ',|\n'
    %% white -> field -> comma -> col
    %% ^--------------------------/ |
    %% end <------------------------'

    case State of
        white ->
            case read_white(IO) of
                eof -> eof;
                R0 -> parse_line(R0, Result, field)
            end;
        comma ->
            case read_white(IO) of
                eof -> eof;
                R0 -> parse_line(R0, Result, col)
            end;
        field ->
            case read_string(IO) of
                {Field, R0} -> parse_line(R0, [Field | Result], comma);
                eof -> {err, {parse_line, eof}}
            end;
        col ->
            case read_to_col(IO) of
                {comma, R0} -> parse_line(R0, Result, white);
                {endl, R0} -> {Result, R0};
                {err, R0} -> {err, {parse_line, R0}};
                eof -> eof
            end
    end.

parse_rec(Pid, IO) ->
    case parse_line(IO, [], white) of
        {[Src, Script, Meta, Checksum, Url], R0} ->
            io:fwrite("Sending: ~s, ~s, ~s, ~s, ~s~n", [Url, Checksum, Meta, Script, Src]),
            Pid ! {req, {Url, Checksum, Meta =/= [], Script =/= [], Src =/= []}},
            parse_rec(Pid, R0);
        eof ->
            io:fwrite("EoF:~n", []),
            Pid ! eof;
        {err, Err} ->
            io:fwrite("Syntax error: ~w~n", [Err]),
            Pid ! {err, {parse_rec, Err}, syntax_error};
        {MissingCols, _} ->
            io:fwrite("Missing: ~w~n", [MissingCols]),
            Pid ! {err, {parse_rec, MissingCols}, missing_columns}
    end.
