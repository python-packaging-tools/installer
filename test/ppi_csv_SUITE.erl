-module(ppi_csv_SUITE).
-compile(export_all).
-include_lib("eunit/include/eunit.hrl").
-include_lib("common_test/include/ct.hrl").

all() -> [rope_test].

rope_flatten(Rope, Flat) ->
    case ppi_csv:rope_next(Rope) of
        {C, []} ->
            lists:reverse([C | Falt]);
        {C, More} ->
            rope_flatten(More, [C | Flat])
    end.

rope_test() ->
    Test = [[["foo", " ", [[], [], [[]]]], "b", ["ar", [" "], "b"], "a"], "z", [], [[]]],
    Flat = "foo bar baz",
    Flat = rope_flatten(Test).
